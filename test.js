var WebSocketServer = require('websocket').server;
var http = require('http');
const fs = require('fs');
var server = http.createServer(function(request, response) {
    // process HTTP request. Since we're writing just WebSockets
    // server we don't have to implement anything.
});
server.listen(1337, function() { });
// list of currently connected clients (users)
var clients = [ ];
users = [];
// create the server
wsServer = new WebSocketServer({
    httpServer: server
});

// WebSocket server
wsServer.on('request', function(request) {
    console.log((new Date()) + ' Connection from origin ' + request.origin + '.');
    var connection = request.accept(null, request.origin);
    console.log((new Date()) + ' Connection accepted.');
    var index = clients.push(connection) - 1;
    console.log(index);
    // remember user name
    // This is the most important callback for us, we'll handle
    // all messages from users here.
    connection.on('message', function(message) {
        console.log(message);
        console.log("New user ID : "+message.utf8Data);
        let data = {
            ID : message.utf8Data,
            IP : request.origin
        };
        fs.appendFileSync('users.json', JSON.stringify(data));
        connection.sendUTF(
            JSON.stringify({type : 'message', data: "Votre ID a bien été enregistré : "+message.utf8Data}));
    });

    connection.on('close', function(connection) {
        // close user connection
        console.log((new Date()) + " Peer "
            + connection.remoteAddress + " disconnected.");
        clients.splice(index, 1);
        console.log(index);
    });
});