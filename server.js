var http = require('http');
var fs = require('fs');
// Chargement du fichier index.html affiché au client
var server = http.createServer(function(req, res) {
    fs.readFile('./index.html', 'utf-8', function(error, content) {
        res.writeHead(200, {"Content-Type": "text/html"});
        res.end(content);
    });
});

var alphabet = "abcdefghijklmnopqrstuvwxyz";
var fullAlphabet = alphabet + alphabet + alphabet;

function runCipher(cipherText,cipherOffset){
    cipherOffset = (cipherOffset % alphabet.length);
    var cipherFinish = '';

    for(i=0; i<cipherText.length; i++){
        var letter = cipherText[i];
        var upper = (letter == letter.toUpperCase());
        letter = letter.toLowerCase();

        var index = alphabet.indexOf(letter);
        if(index == -1){
            cipherFinish += letter;
        } else {
            index = ((index + cipherOffset) + alphabet.length);
            var nextLetter = fullAlphabet[index];
            if(upper) nextLetter = nextLetter.toUpperCase();
            cipherFinish += nextLetter;
        }
    }
    return cipherFinish;
}

messageencrypted = runCipher('bonjour ça va ?', 12);
console.log(messageencrypted);
// Chargement de socket.io
var io = require('socket.io').listen(server);

// Quand un client se connecte, on le note dans la console
io.sockets.on('connection', function (socket) {
    console.log('Un client est connecté !');
    socket.emit('message', 'Vous êtes bien connecté !');

    // Quand le serveur reçoit un signal de type "message" du client
    socket.on('id', function (message) {
        console.log('Un client est connecté avec l\'ID : ' + message);
        socket.id=message;
        socket.broadcast.emit('messageimportant','un nouveau client est : '+socket.id);
        socket.emit('messageencode',messageencrypted);
        for (var i = 0; i < 25; i++) {
            socket.emit('key',i)
        }
    });
});

server.listen(8080);